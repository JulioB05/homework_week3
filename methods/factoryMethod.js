class PostFactory {
  constructor(id, title, subTitle, image, body, createDate, likes, tags) {
    this._id = id;
    this._title = title;
    this._subTitle = subTitle;
    this._image = image;
    this._body = body;
    this._createDate = createDate;
    this._likes = likes;
    this._tags = tags;
  }

  newPost() {
    return `
        <div class="cardAll" >
        <div class ="container"> 
        
        <img class = "imageNew" src="${this._image}" alt="${this._id}">
        
        </div>
        <div class ="details">
        <h3>${this._body}</h3>
        <p> Section: ${this._subTitle} </p>
        <p> Date: ${this._createDate}   Tags: ${this._tags}</p>
        <p> Likes: ${this._likes}</p>       
        <button class="btn-like" id="btn-like" value = "btn-like-${this._id}"> LIKE  </button>
        <button class="btn-delete" id="btn-delete" name = "${this._id}"> DELETE  </button>
        <button class="btn-edit" id="btn-edit" name="${this._id}"> EDIT</a>
        <button class="btn-comment" id="btn-comment" name="${this._id}">COMMENT</button>
        </div>
        </div>
        `;
  }

  newPostRecently() {
    return `
        <div class="cardRecent">
        <div class ="container"> 
      
        <img class = "imageNew" src="${this._image}" alt="${this._id}">
      
        </div>
        <div class ="details">
        <h3>${this._body}</h3>
        <p> Section: ${this._subTitle} </p>
        <p> Date: ${this._createDate}   Tags: ${this._tags}</p>
        <p> Likes: ${this._likes}</p>    
        <button class="btn-like" id="btn-like" name = "${this._id}"> LIKE  </button>
        <button class="btn-delete" id="btn-delete" name = "${this._id}"> DELETE  </button>
        <button class="btn-edit" id="btn-edit" name="${this._id}"> EDIT</a>
        <button class="btn-comment" id="btn-comment" name="${this._id}">COMMENT</button>
        </div>
        </div>
        `;
  }
}

class FactoryTags {
  constructor(id, name, slug) {
    this._id = id;
    this._name = name;
    this._slug = slug;
  }

  newTag() {
    return `
         <div class = "filter-container">
         <button href="#" class="filterLink" id="filterLink" name="${this._id}" value="${this._slug}">${this._id}-${this._name}</button>
        </div>
    `;
  }
}

class FactoryUsers {
  constructor(id, name, lastName) {
    this._id = id;
    this._name = name;
    this._lastName = lastName;
  }

  newUser() {
    return `
         <div class = "filter-container" id="filter-container">
         <button href="#" class="filterLink" id="filterLink" name="user:${this._id}" >${this._name}-${this._lastName}</button>
        </div>
    `;
  }

  newUserAlert() {
    return `
        ID = ${this._id} | ${this._name} ${this._lastName}
        `;
  }
}
