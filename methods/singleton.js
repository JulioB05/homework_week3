class SingletonFetch {
  static dataInstance; //undefined

  constructor() {
    if (!!SingletonFetch.dataInstance) {
      return SingletonFetch.dataInstance;
    }

    SingletonFetch.dataInstance = this;
    this.URL = "https://server-homework3.herokuapp.com";
  }

  async getPost(type) {
    try {
      const response = await fetch(`${this.URL}/${type}`);

      if (response.status === 200) {
        const data = await response.json();
        return data;
      } else if (response.status === 403) {
        console.log("The key is incorrect!");
      }
    } catch (e) {
      console.log(e);
    }
  }

  async postPost(newPost) {
    try {
      const response = await fetch(`${this.URL}/${"posts"}`, {
        headers: {
          Accept: " */*",
          "Content-Type": "application/json",
        },
        method: "POST",
        body: JSON.stringify(newPost),
      });

      if ((response.status === 200) | 201) {
        const data = await response.json();
        return data;
      } else if (response.status === 403) {
        console.log("The key is incorrect!");
      }
    } catch (e) {
      console.log(e);
    }
  }

  async deletePost(postId) {
    try {
      const response = await fetch(`${this.URL}/${"posts"}/${postId}`, {
        headers: {
          "Content-Type": "application/json",
        },
        method: "DELETE",
      });

      if (response.status === 200) {
        const data = await response.json();
        return data;
      } else if (response.status === 403) {
        console.log("The key is incorrect!");
      }
    } catch (e) {
      console.log(e);
    }
  }

  async patchPost(postId, bodyNew) {
    try {
      const response = await fetch(`${this.URL}/${"posts"}/${postId}`, {
        method: "PATCH",
        body: JSON.stringify(bodyNew),
        headers: {
          "Content-type": "application/json; charset=UTF-8",
        },
      });

      if (response.status === 200) {
        const data = await response.json();
        return data;
      } else if (response.status === 403) {
        console.log("The key is incorrect!");
      }
    } catch (e) {
      console.log(e);
    }
  }

  async postComment(newComment) {
    try {
      const response = await fetch(`${this.URL}/${"comments"}`, {
        headers: {
          Accept: " */*",
          "Content-Type": "application/json",
        },
        method: "POST",
        body: JSON.stringify(newComment),
      });

      if ((response.status === 200) | 201) {
        const data = await response.json();
        return data;
      } else if (response.status === 403) {
        console.log("The key is incorrect!");
      }
    } catch (e) {
      console.log(e);
    }
  }
}
