const fetchData = new SingletonFetch();
const firstPage = "";

async function createPost(infoSearch) {
  let data = await fetchData.getPost("posts");
  let orderData = data
    .slice()
    .sort((a, b) => new Date(b.createDate) - new Date(a.createDate));
  let count = 0;
  let posts = "";
  let postsRecently = "";
  let infoSearch2 = infoSearch.toLowerCase();
  orderData.forEach((post) => {
    if (infoSearch2 == "") {
      count += 1;
      if (count <= 3) {
        let newPostRecently = new PostFactory(
          post.id,
          post.title,
          post.subTitle,
          post.image,
          post.body,
          post.createDate,
          post.likes,
          post.tags
        );
        let structurePostRecently = newPostRecently.newPostRecently();
        postsRecently += structurePostRecently;
      } else {
        let newPost = new PostFactory(
          post.id,
          post.title,
          post.subTitle,
          post.image,
          post.body,
          post.createDate,
          post.likes,
          post.tags
        );
        let structurePost = newPost.newPost();
        posts += structurePost;
      }
    } else {
      let body = post.body.toLowerCase();
      if (body.indexOf(infoSearch2) !== -1) {
        count += 1;
        if (count <= 3) {
          let newPostRecently = new PostFactory(
            post.id,
            post.title,
            post.subTitle,
            post.image,
            post.body,
            post.createDate,
            post.likes,
            post.tags
          );
          let structurePostRecently = newPostRecently.newPostRecently();
          postsRecently += structurePostRecently;
        } else {
          let newPost = new PostFactory(
            post.id,
            post.title,
            post.subTitle,
            post.image,
            post.body,
            post.createDate,
            post.likes,
            post.tags
          );
          let structurePost = newPost.newPost();
          posts += structurePost;
        }
      }
    }
  });

  //Error de busqueda
  if (count < 1) {
    document.getElementById("notMatch").classList.add("notMatch_activate");
    document.getElementById(
      "notMatch"
    ).innerHTML = `Sorry! Not match for: "${infoSearch}"`;
  } else {
    document.getElementById("notMatch").classList.remove("notMatch_activate");
  }

  document.getElementById("post-container-all").innerHTML = posts;
  document.getElementById("post-container-recently").innerHTML = postsRecently;
}

//Creando tags para desplegar
async function createTags() {
  let data = await fetchData.getPost("tags");
  let orderData = data.sort();
  let tags = "";

  orderData.forEach((tag) => {
    let newTag = new FactoryTags(tag.id, tag.name, tag.slug);
    let structureTag = newTag.newTag();
    tags += structureTag;
  });
  document.getElementById("navbar").innerHTML = tags;
}

//solucionando creacion de dom para luego crear listeners

window.addEventListener("load", async function (event) {
  await createPost(firstPage);
  await createTags();

  listenerDel();
  listenerTag();
  listenerEdit();
  likePost();
  commentPost();
});
