const search = document.querySelector("#search");
// Debounce function
const debounce = (fn, delay) => {
  let timeoutID;
  return function (...args) {
    if (timeoutID) {
      clearTimeout(timeoutID);
    }
    timeoutID = setTimeout(() => {
      fn(...args);
    }, delay);
  };
};

//Search
search.addEventListener(
  "input",
  debounce((e) => {
    localStorage.setItem("searchUser", e.target.value);
    infoSearch = e.target.value;

    createPost(infoSearch);
  }, 1000)
);
