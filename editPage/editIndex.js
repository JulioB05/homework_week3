const fetchData = new SingletonFetch();
const form = document.getElementById("form-btn");
const inputs = document.querySelectorAll("#form");
const deleteEdit = document.querySelector("#btn-deleteEdit");

const expressions = {
  title: /^[0-9a-zA-ZÀ-ÿ\s]{1,200}$/, // Letras y espacios, pueden llevar acentos.
  subTitle: /^[0-9a-zA-ZÀ-ÿ\s]{1,200}$/, // Letras y espacios, pueden llevar acentos.
  author: /^\d+$/, // Letras y espacios, pueden llevar acentos.
  createDate: /^\d{4}\/\d{2}\/\d{2}$/,
  body: /^(.|\s)*[a-zA-Z]+(.|\s)*$/, // Letras y espacios, pueden llevar acentos.
  tags: /^(\d+)(,\s*\d+)*/,
  image:
    /[(http(s)?):\/\/(www\.)?a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/,
};
let errorCount = 0;
const errValidate = [
  "title",
  "subTitle",
  "author",
  "createDate",
  "body",
  "tags",
  "image",
];
const fields = {
  title: false,
  subTitle: false,
  author: false,
  createDate: false,
  body: false,
  tags: false,
  image: false,
};

const DataPost = {
  title: "",
  subTitle: "",
  author: "",
  createDate: "",
  body: "",
  tags: [],
  image: "",
};

const formValidate = ({ target: { name, value } }) => {
  switch (name) {
    case name:
      validateInfo(expressions[name], value, name);
      break;
  }
};

const validateInfo = (expression, input, field) => {
  if (expression.test(input)) {
    document
      .getElementById(`group_${field}`)
      .classList.remove("group_form_incorrect");
    document
      .getElementById(`group_${field}`)
      .classList.add("group_form_correct");
    document
      .querySelector(`#group_${field} .form_input_err`)
      .classList.remove("form_input_err_active");
    fields[field] = true;

    if (field == "tags") {
      let strTag = input;
      DataPost[field] = strTag.split(",");
    } else {
      DataPost[field] = input;
    }
  } else {
    document
      .getElementById(`group_${field}`)
      .classList.add("group_form_incorrect");
    document
      .getElementById(`group_${field}`)
      .classList.remove("group_form_correct");
    document
      .querySelector(`#group_${field} .form_input_err`)
      .classList.add("form_input_err_active");
    fields[field] = false;
  }
};

inputs.forEach((input) => {
  input.addEventListener("keyup", formValidate); //validacion cuando levanta tecla
  input.addEventListener("blur", formValidate); //validacion cuando da click fuera
});

async function editPost(id, body) {
  let data = await fetchData.patchPost(id, body);
  console.log("actualizado Correctamente " + data);
}

//Btn delete of edit page
deleteEdit.addEventListener("click", async (e) => {
  let id = localStoData.id;
  if (confirm("Are you sure you want to delete this post?")) {
    await fetchData.deletePost(id);
    alert("The post has been deleted");
    location.href = "../index.html";
  }
});

form.addEventListener("submit", (e) => {
  e.preventDefault();

  if (
    fields.title &&
    fields.subTitle &&
    fields.author &&
    fields.createDate &&
    fields.body &&
    fields.tags &&
    fields.image == true
  ) {
    form.reset();

    editPost(localStoData.id, DataPost);

    document
      .getElementById("form_message_send")
      .classList.add("form_message_send_active");

    setTimeout(() => {
      document
        .getElementById("form_message_send")
        .classList.remove("form_message_send_active");
    }, 4000);

    document.querySelectorAll(".group_form_correct").forEach((greenCheck) => {
      greenCheck.classList.remove("group_form_correct");
    });

    document
      .getElementById("form_message")
      .classList.remove("form_message_active");
    //location.href = "../index.html";
  } else {
    document
      .getElementById("form_message")
      .classList.add("form_message_active");
  }
});
