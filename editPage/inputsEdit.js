let localStoData = {
  title: localStorage.getItem("title"),
  subTitle: localStorage.getItem("subtitle"),
  author: localStorage.getItem("author"),
  createDate: localStorage.getItem("createDate"),
  body: localStorage.getItem("body"),
  tags: localStorage.getItem("tags"),
  image: localStorage.getItem("image"),
  id: localStorage.getItem("id"),
};

console.log(localStoData);

let inputsData = [
  {
    typeInput: "text",
    type: "title",
    name: "TITLE",
    value: `${localStoData.title}`,
    err: "Complete correctly",
    image: `${localStoData.image}`,
  },
  {
    typeInput: "text",
    type: "subTitle",
    name: "SUBTITLE",
    value: `${localStoData.subTitle}`,
    err: "Complete correctly",
    image: `${localStoData.image}`,
  },
  {
    typeInput: "text",
    type: "author",
    name: "AUTHOR",
    value: `${localStoData.author}`,
    err: "Complete correctly",
    image: `${localStoData.image}`,
  },
  {
    typeInput: "date",
    type: "createDate",
    name: "CREATE DATE",
    value: `${localStoData.createDate}`,
    err: "Complete correctly",
    image: `${localStoData.image}`,
  },
  {
    typeInput: "text",
    type: "body",
    name: "BODY",
    value: `${localStoData.body}`,
    err: "Complete correctly",
    image: `${localStoData.image}`,
  },
  {
    typeInput: "number",
    type: "tags",
    name: "TAGS",
    value: `${localStoData.tags}`,
    err: "Complete correctly",
    image: `${localStoData.image}`,
  },
  {
    typeInput: "url",
    type: "image",
    name: "IMAGE",
    value: `${localStoData.image}`,
    err: "Complete correctly",
    image: `${localStoData.image}`,
  },
];

function createEditInputs() {
  let inputs = "";
  let imageEdit = "";
  inputsData.forEach((input) => {
    let newInput = new FactoryEditInput(
      input.typeInput,
      input.type,
      input.name,
      input.value,
      input.err,
      input.image
    );
    let structureEditInput = newInput.newEditInput();
    let imageEditInput = newInput.newImageEdit();
    inputs += structureEditInput;
    imageEdit = imageEditInput;
  });

  document.getElementById("form").innerHTML = inputs;
  document.getElementById("img_container").innerHTML = imageEdit;
}

createEditInputs();
