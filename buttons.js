// Function for delete post
function listenerDel() {
  const btnDel = document.querySelectorAll("#btn-delete");
  const fetchData = new SingletonFetch();

  btnDel.forEach((btn) => {
    btn.addEventListener("click", async (e) => {
      if (confirm("Are you sure you want to delete this post?")) {
        await fetchData.deletePost(e.target.name);
        alert("The post has been deleted");
        location.href = "./index.html";
      } else {
        //location.href = "./index.html";
      }
    });
  });
}

// Function for selects tags
function listenerTag() {
  const btnTag = document.querySelectorAll("#filterLink");
  const fetchData = new SingletonFetch();
  let tagsSelects = [];
  let count = 0;

  btnTag.forEach((btn) => {
    btn.addEventListener("click", (e) => {
      let tagSelect = e.target.name;
      //console.log(e.target.value);

      tagsSelects.forEach((tag) => {
        if (tag == tagSelect) {
          count += 1;
        }
      });

      if (count > 0) {
        let index = tagsSelects.indexOf(tagSelect);
        tagsSelects.splice(index, 1);
        count = 0;
      } else {
        tagsSelects.push(tagSelect);
      }

      //console.log(tagsSelects);

      //createPostFilter(tagsSelects);
    });
  });
}

//Function for edit post button
function listenerEdit() {
  const btnEdit = document.querySelectorAll("#btn-edit");
  const fetchData = new SingletonFetch();

  btnEdit.forEach((btn) => {
    btn.addEventListener("click", async (e) => {
      console.log(e.target.name);
      let data = await fetchData.getPost(`posts/${e.target.name}`);

      let bodyInfo = {
        title: data.title,
        subTitle: data.subTitle,
        author: data.author,
        createDate: data.createDate,
        body: data.body,
        image: data.image,
        tags: data.tags,
        id: data.id,
      };

      localStorage.setItem("title", bodyInfo.title);
      localStorage.setItem("subtitle", bodyInfo.subTitle);
      localStorage.setItem("author", bodyInfo.author);
      localStorage.setItem("createDate", bodyInfo.createDate);
      localStorage.setItem("body", bodyInfo.body);
      localStorage.setItem("image", bodyInfo.image);
      localStorage.setItem("tags", bodyInfo.tags);
      localStorage.setItem("id", bodyInfo.id);

      location.href = "./editPage/editIndex.html";
    });
  });
}

//function for likes

function likePost() {
  const btnLike = document.querySelectorAll("#btn-like");
  const fetchData = new SingletonFetch();

  btnLike.forEach((btn) => {
    btn.addEventListener("click", async (e) => {
      let data = await fetchData.getPost(`posts/${e.target.name}`);
      let likes = Number(data.likes);

      likes += 1;

      await fetchData.patchPost(e.target.name, { likes: `${likes}` });
      location.href = "./index.html";
    });
  });
}

//function for comments
function commentPost() {
  const btnComment = document.querySelectorAll("#btn-comment");
  const fetchData = new SingletonFetch();

  btnComment.forEach((btn) => {
    btn.addEventListener("click", async (e) => {
      let data = await fetchData.getPost("users");
      let users = "";

      data.forEach((user) => {
        let newUser = new FactoryUsers(user.id, user.name, user.lastName);
        let structureNewUser = newUser.newUserAlert();
        users += structureNewUser;
      });

      alert("Would you like to make a comment?");

      let userInput = prompt(`What "ID" is yours? ${users}`, "1");
      let commentInfo = prompt(
        "Hi, You can write your comment: ",
        "This is your comment"
      );
      let reponseConfirm = confirm(
        `You confirm that you are the "ID" = ${userInput} and your comment is: ${commentInfo}`
      );

      let comment = {
        comment: commentInfo,
        postID: Number(e.target.name),
        user: Number(userInput),
      };
      if (reponseConfirm) {
        await fetchData.postComment(comment);
      }
    });
  });
}

async function createPostFilter(tags) {
  let data = await fetchData.getPost("posts");
  let count = 0;
  let posts = "";
  let postsRecently = "";
  let tags2 = JSON.parse("[" + tags + "]");

  data.forEach((post) => {
    count += 1;
    // console.log(post.tags);
    // console.log(tags2.length);

    //  if (post.tags.includes(tags2)) {
    //    if (count <= 3) {
    //      let newPostRecently = new PostFactory(post.id, post.title, post.subTitle, post.image, post.body, post.createDate, post.likes, post.tags);
    //      let structurePostRecently = newPostRecently.newPostRecently();
    //      postsRecently += structurePostRecently;

    //    } else {
    //      let newPost = new PostFactory(post.id, post.title, post.subTitle, post.image, post.body, post.createDate, post.likes, post.tags);
    //      let structurePost = newPost.newPost();
    //      posts += structurePost;
    //    }

    // }
    // document.getElementById("post-container-all").innerHTML = posts;
    // document.getElementById("post-container-recently").innerHTML = postsRecently;
  });
}
