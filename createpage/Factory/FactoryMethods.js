class FactoryInput {
  constructor(typeInput, type, name, placeHolder, err) {
    this._type = typeInput;
    this._type = type;
    this._name = name;
    this._placeHolder = placeHolder;
    this._err = err;
  }

  newInput() {
    return `
        <div class="group_form" id="group_${this._type}">
          <label for="${this._type}" class="form_label">${this._name}</label>
          <div class="group_form_input">
            <input
              type="${this._typeInput}"
              class="form_input"
              name="${this._type}"
              id="${this._type}"
              placeholder="${this._placeHolder}"
            />
          </div>
          <p class="form_input_err">
            ${this._err}
          </p>
          </div>
      `;
  }
}

class FactoryEditInput {
  constructor(typeInput, type, name, value, err, image) {
    this._type = typeInput;
    this._type = type;
    this._name = name;
    this._value = value;
    this._err = err;
    this._image = image;
  }

  newEditInput() {
    return `
        <div class="group_form" id="group_${this._type}">
          <label for="${this._type}" class="form_label">${this._name}</label>
          <div class="group_form_input">
            <input
              type="${this._typeInput}"
              class="form_input"
              name="${this._type}"
              id="${this._type}"
              value="${this._value}"
            />
          </div>
          <p class="form_input_err">
            ${this._err}
          </p>
          </div>
      `;
  }

  newImageEdit() {
    return `
    <img src="${this._image}" alt="${this._name}">
    `;
  }
}

class pageOfPost {
  constructor(id, title, subTitle, image, body, createDate, likes, comments) {
    this._id = id;
    this._title = title;
    this._subTitle = subTitle;
    this._image = image;
    this._body = body;
    this._createDate = createDate;
    this._likes = likes;
    this._comments = comments;
  }
  newPostPage() {
    return `
  <div class="cardRecent">
  <div class ="container"> 
  <img class = "imageNew" src="${this._image}" alt="${this._id}">
  
  </div>
  <div class ="details">
  <h3>${this._body}</h3>
  <p> Section: ${this._subTitle} </p>
  <p> Date: ${this._createDate}</p>
  <p> Likes: ${this._likes}</p>    
  <p> Comments: ${this._comments}</p>    
  
  <button class="btn-like" id="btn-like" name = "${this._id}"> LIKE  </button>
  <button class="btn-delete" id="btn-delete" name = "${this._id}"> DELETE  </button>
  <button class="btn-edit" id="btn-edit" name="${this._id}"> EDIT</a>
  <button class="btn-comment" id="btn-comment" name="${this._id}">COMMENT</button>
  </div>
  </div>
  `;
  }
}
