let inputsData = [
  {
    typeInput: "text",
    type: "title",
    name: "TITLE",
    placeHolder: "Mario",
    err: "Complete correctly",
  },
  {
    typeInput: "text",
    type: "subTitle",
    name: "SUBTITLE",
    placeHolder: "Smash Bros",
    err: "Complete correctly",
  },
  {
    typeInput: "text",
    type: "author",
    name: 'AUTHOR ID: "1" ',
    placeHolder: "1-JohnWick 2-JohnCena 3-BruceWayne",
    err: "Complete correctly",
  },
  {
    typeInput: "date",
    type: "createDate",
    name: "CREATE DATE",
    placeHolder: "2020/11/01",
    err: "Complete correctly",
  },
  {
    typeInput: "text",
    type: "body",
    name: "BODY",
    placeHolder: "Description",
    err: "Complete correctly",
  },
  {
    typeInput: "number",
    type: "tags",
    name: "TAGS",
    placeHolder: "1,2",
    err: "Complete correctly",
  },
  {
    typeInput: "url",
    type: "image",
    name: "IMAGE",
    placeHolder: "Copy URL",
    err: "Complete correctly",
  },
];

function createInputs() {
  let inputs = "";
  inputsData.forEach((input) => {
    let newInput = new FactoryInput(
      input.typeInput,
      input.type,
      input.name,
      input.placeHolder,
      input.err
    );
    let structureNewInput = newInput.newInput();
    inputs += structureNewInput;
  });
  document.getElementById("form").innerHTML = inputs;
}

createInputs();
